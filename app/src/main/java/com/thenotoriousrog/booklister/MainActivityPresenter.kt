package com.thenotoriousrog.booklister

import android.support.design.widget.Snackbar
import com.thenotoriousrog.booklister.backend.Book
import com.thenotoriousrog.booklister.backend.BookParser

class MainActivityPresenter(private val updater: MainActivityUpdater) {

    fun fetchBooks() {
        BookParser(this).start()
        updater.startRefreshing()
    }

    fun updateBooks(books: List<Book>?) {

        when {
            books == null -> updater.displaySnackbarMessage(R.string.parse_books_error, Snackbar.LENGTH_INDEFINITE)
            books.isEmpty() -> updater.displaySnackbarMessage(R.string.no_books_message, Snackbar.LENGTH_INDEFINITE)
            else -> updater.updateBooksList(books)
        }

        updater.stopRefreshing()
    }

}