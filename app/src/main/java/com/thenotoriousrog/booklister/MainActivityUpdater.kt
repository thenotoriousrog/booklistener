package com.thenotoriousrog.booklister

import com.thenotoriousrog.booklister.backend.Book

interface MainActivityUpdater {

    /**
     * Displays the progress bar
     */
    fun startRefreshing()

    /**
     * Hides the progress bar
     */
    fun stopRefreshing()

    /**
     * Updates the recycler view of books when ready
     */
    fun updateBooksList(books: List<Book>)

    /**
     * Displays a snackbar message to display to users
     * @param message - the message displayed via a resource i.e. R.string.some_string_resource
     * @param messageLength - the length of the message. Either Snackbar.LENGTH_LONG, Snackbar.LENGTH_SHORT, or Snackbar.LENGTH_INDEFINITE
     */
    fun displaySnackbarMessage(message: Int, messageLength: Int)
}