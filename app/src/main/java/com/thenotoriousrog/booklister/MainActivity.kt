package com.thenotoriousrog.booklister

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import com.thenotoriousrog.booklister.backend.Book
import com.thenotoriousrog.booklister.bookitemrecycler.BookItemRecyclerAdapter
import com.thenotoriousrog.booklister.bookitemrecycler.BookItemRecyclerPresenter

class MainActivity : AppCompatActivity(), MainActivityUpdater {

    private lateinit var mainActivityContainer: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewAdapter: BookItemRecyclerAdapter
    private lateinit var recyclerViewPresenter: BookItemRecyclerPresenter
    private val presenter = MainActivityPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.mainActivityContainer = findViewById(R.id.main_activity_container)
        this.progressBar = findViewById(R.id.progress_bar)
        this.recyclerView = findViewById(R.id.recycler_view)

        this.recyclerViewPresenter = BookItemRecyclerPresenter(ArrayList())
        this.recyclerViewAdapter = BookItemRecyclerAdapter(this.recyclerViewPresenter)
        this.recyclerView.layoutManager = LinearLayoutManager(this)
        this.recyclerView.adapter = this.recyclerViewAdapter
    }

    override fun onStart() {
        super.onStart()
        this.presenter.fetchBooks() // wait for the UI to be visible to the user
    }

    override fun startRefreshing() {
        runOnUiThread {
            this.progressBar.visibility = View.VISIBLE
        }
    }

    override fun stopRefreshing() {
        runOnUiThread {
            this.progressBar.visibility = View.GONE
        }
    }

    override fun updateBooksList(books: List<Book>) {
        this.recyclerViewPresenter.updateBookItems(books)
        runOnUiThread {
            this.recyclerViewAdapter = BookItemRecyclerAdapter(recyclerViewPresenter)
            this.recyclerView.adapter = this.recyclerViewAdapter
        }
    }

    override fun displaySnackbarMessage(message: Int, messageLength: Int) {
        Snackbar.make(mainActivityContainer, getString(message), messageLength).show()
    }
}
