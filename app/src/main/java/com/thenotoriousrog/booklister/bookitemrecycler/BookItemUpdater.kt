package com.thenotoriousrog.booklister.bookitemrecycler

/**
 * Used to update book items within BookItemViewHolder
 */
interface BookItemUpdater {

    fun setTitle(title: String?)
    fun setAuthor(author: String?)
    fun setImageURL(imageURL: String?)
}