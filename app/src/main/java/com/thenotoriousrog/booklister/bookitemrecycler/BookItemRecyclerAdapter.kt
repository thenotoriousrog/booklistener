package com.thenotoriousrog.booklister.bookitemrecycler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.thenotoriousrog.booklister.R
import java.text.FieldPosition

class BookItemRecyclerAdapter(private val presenter: BookItemRecyclerPresenter) : RecyclerView.Adapter<BookItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookItemViewHolder {
        return BookItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.book_item, parent, false))
    }

    override fun onBindViewHolder(holder: BookItemViewHolder, position: Int) {
        presenter.onBindBookItemAtPosition(position, holder)
    }

    override fun getItemCount(): Int {
        return presenter.getBookItemCount()
    }
}