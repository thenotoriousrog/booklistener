package com.thenotoriousrog.booklister.bookitemrecycler

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.thenotoriousrog.booklister.R

class BookItemViewHolder(bookItem: View) : RecyclerView.ViewHolder(bookItem), BookItemUpdater {

    private val titleText: TextView = bookItem.findViewById(R.id.book_title)
    private val authorText: TextView = bookItem.findViewById(R.id.book_author)
    private val imageView: ImageView = bookItem.findViewById(R.id.book_image)

    override fun setTitle(title: String?) {
        this.titleText.text = title ?: itemView.context.getString(R.string.no_title)
    }

    override fun setAuthor(author: String?) {
        this.authorText.text = author ?: itemView.context.getString(R.string.unknown_author)
    }

    override fun setImageURL(imageURL: String?) {

        Picasso.get()
            .load(imageURL)
            .error(R.drawable.ic_question_mark)
            .into(imageView)
    }

}