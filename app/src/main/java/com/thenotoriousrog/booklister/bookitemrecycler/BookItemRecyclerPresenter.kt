package com.thenotoriousrog.booklister.bookitemrecycler

import com.thenotoriousrog.booklister.backend.Book

class BookItemRecyclerPresenter(private var bookItems: List<Book>) {

    fun onBindBookItemAtPosition(pos: Int, updater: BookItemUpdater) {
        val book = this.bookItems[pos]
        updater.setTitle(book.getTitle())
        updater.setAuthor(book.getAuthor())
        updater.setImageURL(book.getImageURL())
    }

    fun getBookItemCount(): Int {
        return this.bookItems.size
    }

    fun updateBookItems(bookItems: List<Book>) {
        this.bookItems = bookItems
    }
}