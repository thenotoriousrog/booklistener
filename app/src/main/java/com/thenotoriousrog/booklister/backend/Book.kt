package com.thenotoriousrog.booklister.backend

import com.google.gson.annotations.SerializedName

/**
 * Defines all of the items associated with a book.
 * This class is generated through the use of Gson.
 */
class Book {

    @SerializedName("title") private var title: String? = null
    @SerializedName("author") private var author: String? = null
    @SerializedName("imageURL") private var imageURL: String? = null

    fun setTitle(title: String?) { this.title = title }
    fun setAuthor(author: String?) { this.author = author }
    fun setImageURL(imageURL: String?) { this.imageURL = imageURL }

    fun getTitle(): String? { return this.title }
    fun getAuthor(): String? { return this.author }
    fun getImageURL(): String? { return this.imageURL }
}