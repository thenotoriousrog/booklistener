package com.thenotoriousrog.booklister.backend

import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.thenotoriousrog.booklister.MainActivityPresenter
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

/**
 * This thread is responsible for downloading and parsing the books from the link(s) that exist inside Constants
 * Creates a list of Books if available and returns it back to the MainPresenter upon completion
 */
class BookParser(private val presenter: MainActivityPresenter) : Runnable {

    private val TAG = "BookParser"
    private val backgroundThread = Thread(this)

    fun start() {
        this.backgroundThread.start()
    }

    override fun run() {
        val gson = GsonBuilder()
            .setDateFormat("M/d/yy hh:mm a")
            .create()

        try {

            val httpClient = OkHttpClient()
            val request = Request.Builder()
                .url(Constants.BOOK_LIST)
                .build()

            val response = httpClient.newCall(request).execute()
            val jsonData = response.body()?.string()
            if(jsonData != null) {
                val books = gson.fromJson<List<Book>>(jsonData, object : TypeToken<List<Book>>() {}.type)
                presenter.updateBooks(books)
            } else {
                Log.w(TAG, "The jsonData returned from the books link is null!! Response: $response")
                presenter.updateBooks(ArrayList()) // return an empty list which will alert the users that no books were found
            }

        } catch (exception: IOException) {
            exception.printStackTrace()
            Log.e(TAG, "Caught an IOException inside BookParser: ${exception.message}")
            presenter.updateBooks(null)
        }

    }
}