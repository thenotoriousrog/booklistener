# BookLister (POSSIBLEMobile coding challenge)

## How to run
	* Clone this repo
	* Open it in Android Studio or whatever IDE that you love most
	* Wait for gradle to finish running then try to run the project which will generate a build version of this apk.
	* You may also choose to download the apk available in the repository.

## Things to know
	* The app is targed for API level 28 (does run on Android Q Preview right now)
	* The min SDK is 23 for simplicity for me as the dev. Most "newer" devices should have no problem running the app.
	* This app requires an internet connection to properly work.

	